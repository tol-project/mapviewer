source "../tcl/RangeTable.tcl"
source "../tcl/GeoVariable.tcl"

GeoVariable geo_var

for { set i 0 } { $i < 100 } { incr i } {
  geo_var SetValue "id_$i" $i
}

set Raws [lsort -real [ geo_var GetRawValues ]]

set names [ geo_var GetEntityNames ]
puts "[llength $names]"

RangeTable look1
look1 BuildTable nquantiles -values $Raws -size 10
puts "look1 = [ look1 GetTable ]"

puts "0 is in [look1 FindInterval 0]"
puts "1 is in [look1 FindInterval 1]"
puts "[lindex $Raws end] is in [look1 FindInterval [lindex $Raws end] ]"
puts "50 is in [look1 FindInterval 50 ]"

RangeTable look2
look2 BuildTable uniform -values $Raws -size 10
puts "look2 = [ look2 GetTable ]"

RangeTable look3
look3 BuildTable uniform -minmax {0 99} -size 10
puts "look3 = [ look3 GetTable ]"

RangeTable look4
look4 BuildTable absolute -values {0 10 20 30 40 50 60 70 80 90 99}
puts "look4 = [ look4 GetTable ]"

RangeTable look5
look5 BuildTable quantiles -values {0 10 20 30 40 50 60 70 80 90 99} -prob {0.1 0.5 0.9}
puts "look5 = [ look5 GetTable ]"
puts "0 is in [look5 FindInterval 0]"
puts "1 is in [look5 FindInterval 1]"
puts "[lindex $Raws end] is in [look5 FindInterval [lindex $Raws end] ]"
puts "50 is in [look5 FindInterval 50 ]"
