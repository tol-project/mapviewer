source "Test_Conf.tcl"
source "../tcl/RegionShape.tcl"

set obj [ RegionShape %AUTO% \
              -shpfile "$DataPath/SpainMaps/esp_muni_00" \
              -idfield "PROVMUN" \
              -namefield "MUNICIPIO0" \
              -level "Municipios" ]

puts "[ lrange [$obj GetEntityCodes] 0 10 ]"

foreach c [$obj GetEntityCodes] {
  if { [ string length $c ] < 5 } {
    puts $c
  }
  if { [ string index $c 0 ] eq "0" } {
    puts $c
  }
}


