//////////////////////////////////////////////////////////////////////////////
// FILE   : MapViewer.tol
// PURPOSE: Package to plot a spatial variable on a Map.
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
NameBlock MapViewer =
//////////////////////////////////////////////////////////////////////////////
[[
#Require GuiTools;
  
//read only autodoc
Text _.autodoc.name = "MapViewer";
Text _.autodoc.brief =
"Plot a spatial variable on a Map.";
Text _.autodoc.description =
"Plot a spatial variable on a Map. The maps should be provided as shapefiles";
Text _.autodoc.url = 
"http://packages.tol-project.org/OfficialTolArchiveNetwork/repository.php";
Set _.autodoc.keys = [[ "Spatial", "GIS", "Plot" ]];
Set _.autodoc.authors = [[ "josp@tol-project.org", "javipor@bayesinf.com"]];
Text _.autodoc.minTolVersion = "v2.0.1 b.1";
Real _.autodoc.version.high = 2;
Real _.autodoc.version.low = 3;
Set _.autodoc.dependencies = Copy(Empty);
Set _.autodoc.nonTolResources = [[
  Text "tcl",
  Text "lib"
]] ;
Text _.autodoc.versionControl = AvoidErr.NonDecAct(OSSvnInfo("."));

#Embed "DefineGeoVariable.tol";
#Embed "PlotFunctions.tol";
#Embed "GuiResources.tol";

Real _is_started = False;
////////////////////////////////////////////////////////////////////////////
Real StartActions(Real void)
////////////////////////////////////////////////////////////////////////////
{
  If(_is_started, False, {
      Real _is_started := True;
      Text cwd = GetAbsolutePath( "." ) + "/";
      If( GuiTools::InsideTk(?), {
          Set tcl = Tcl_Eval( "source tcl/Init.tcl" );
          Real If( tcl["status"], {
              GuiResources::Define(?)
            }, {
              Error( "MapViewer::StartActions :" + tcl["result"] );
              False
            } )
        }, {
          Warning( "MapViewer will not plot because Tk is not available." );
          True
        } )
    })
}

]];

/*
MapViewer::@GeoVariable varCobADSL1 = MapViewer::@GeoVariable::New(
[[
  Text _.name        = "Penetracion ADSL Movistar Fijo en 2010-12 (prueba)";
  Text _.regionLevel = "Municipios";
  Set _.values       =  { [[ Real barcelona = 1, Real madrid = 2 ]] };
  Set _.rangeDef     = { [[ Text type = "Uniform", Real size = 4 ]] };
  Set _.colorDef     = { [[ Text type = "Table",   Set colors = [[ "brown", "red", "orange", "yellow" ]] ]] }
]]);
*/
