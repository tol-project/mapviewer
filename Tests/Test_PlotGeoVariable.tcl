namespace eval gistest {
  variable cwd

  set cwd [ file normal  [ file dir  [ info script ] ] ]
}

proc Tol_HciWriter { msg } {
  puts [ string trim $msg ]
}

if { [ info command tol::initkernel ] eq "" } {
  set v [ package require Toltcl ]
  tol::initkernel
  if { $v eq "2.0.2" } {
    tol::initlibrary 1
  } else {
    tol::initlibrary
  }
}

source [ file join $gistest::cwd "../tcl" Init.tcl ]

proc gistest::Load_SSCC_P18 { { decompile 0 } } {
  set P18 [ file join $gistest::cwd .. .. SSCC_P18_Maps "SSCC_P18_Maps.tol" ]
  if { $decompile } {
    catch {
      tol::decompile $P18
    }
  }
  if { [ catch { tol::info var {NameBlock SSCC_P18_Maps} } ] } {
    tol::include $P18
  }
  tol::console eval {#Require SSCC_P18_Maps}
}

proc gistest::LoadSpainMaps { } {
  tol::console eval {#Require SpainMaps}
}

proc gistest::LoadMapViewer { { decompile 0 } } {
  set viewerCode [ file join $gistest::cwd .. "MapViewer.tol" ]
  if { $decompile } {
    catch {
      tol::decompile $viewerCode
    }
  }
  if { [ catch { tol::info var {NameBlock MapViewer} } ] } {
    tol::include $viewerCode
  }
  tol::console eval {#Require MapViewer}
}

proc gistest::LoadTestGeoVariable_3G { } {
  variable cwd
  variable objAddr_3G

  LoadMapViewer
  LoadSpainMaps
  set test_file [ file join $gistest::cwd "Test_GeoVariable.tol" ]
  catch {
    tol::decompile $test_file
  }
  tol::include $test_file
  tol::console eval {#Require SpainMaps}
  set objAddr_3G [ toltcl::eval { Text GetAddressFromObject( varCobADSL1 ) } ]
}

proc gistest::LoadTestGeoVariable_SEUR { } {
  variable cwd
  variable objAddr_SEUR

  Load_SSCC_P18
  LoadSpainMaps  
  LoadMapViewer
  set test_file [ file join $gistest::cwd "Test_GeoVariable_P18.tol" ]
  catch {
    tol::decompile $test_file
  }
  tol::include $test_file
  tol::console eval {#Require SpainMaps}
  set objAddr_SEUR [ toltcl::eval { Text GetAddressFromObject( sscc_P18 ) } ]
}

proc Test_MapsInfo { map } {
  foreach level [ ::MapViewer::GetMapLevels $map ] {
    puts [ ::MapViewer::GetMapInfo $map $level ]
  }
}

proc Test_SpainMaps { } {
  Test_MapsInfo "SpainMaps"
}

proc Test_SSCC_P18 { } {
  Load_SSCC_P18
  Test_MapsInfo "SSCC_P18_Maps"
}

proc Test_GeoVariable_3G { } {
  puts [ ::MapViewer::GetInfoFromVariable $gistest::objAddr_3G ]
}

proc Test_GeoVariable_SEUR { } {
  puts [ ::MapViewer::GetInfoFromVariable $gistest::objAddr_SEUR ]
}

proc Test_Plot_3G { } {
  MapViewer::PlotGeoVariable $gistest::objAddr_3G
}

proc Test_Plot_SEUR { } {
  MapViewer::PlotGeoVariable $gistest::objAddr_SEUR
}