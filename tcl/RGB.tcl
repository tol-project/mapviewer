package require Tk
package require snit

namespace eval RGB {

  proc _DrawLegendRectangleY { c color pos1 pos2 w } {
    $c create rectangle 0 $pos1 $w $pos2 -tags legend \
        -fill $color -width 0
  }

  proc _DrawLegendText { c x y font str } {
    $c create text $x $y -text $str \
        -font $font -tags legend -anchor w
  }
  
  proc _ComputeLegendSize { colorTable maxSize legend args } {
    array set opts {
      -fontsize 0
    }
    array set opts $args
    if {$::tcl_platform(platform) eq "windows"} {
      set fontPropList {-family {Lucida Console}}
      if {$opts(-fontsize)} {
        lappend fontPropList -size $opts(-fontsize)
      } else {
        lappend fontPropList -size 8
      }
    } else {
      array set fontProp [font configure TkDefaultFont]
      if {$opts(-fontsize)} {
        set fontProp(-size) $opts(-fontsize)
      } else {
        set fontProp(-size) 6
      }
      set fontPropList [array get fontProp]
    }
    array set fontMetric [font metric $fontPropList]
    set textHeight $fontMetric(-linespace)
    set tableSize [$colorTable GetSize]
    set size [expr {$maxSize>$tableSize?$tableSize:$maxSize}]
    set rectHeight [expr {$textHeight+2}]
    set height [expr {$rectHeight*($size+1)}]
    set textWidth 0
    foreach v $legend {
      set tw [font measure $fontPropList $v]
      if {$tw>$textWidth} {
        set textWidth $tw
      }
    }
    return [list -size $size -rectheight $rectHeight -textwidth $textWidth -height $height -font $fontPropList]
  }

  proc UpdateLegendCanvas { c colorTable args } {
    array set opts {
      -force no
      -maxsize 10
      -width 20
      -values ""
      -labels ""
      -fontsize 0
    }
    array set opts $args
    set ll [llength $opts(-labels)]
    if {$ll} {
      if {$ll != [$colorTable GetSize]} {
        error "Legend table ($ll) should match the size of the color table ([$colorTable GetSize])"
      }
      set opts(-maxsize) $ll
      set legend $opts(-labels)
    } else {
      set legend {}
      foreach v $opts(-values) {
        lappend legend [format "%.6g" $v]
      }
    }
    array set geom [_ComputeLegendSize $colorTable \
                        $opts(-maxsize) $legend \
                        -fontsize $opts(-fontsize)]
    set width [expr {$opts(-width) + $geom(-textwidth) + 6}]
    $c configure -width $width -height $geom(-height)
    $c delete legend

    set length [expr {$opts(-width)-1}]
    set textPos [expr {$opts(-width)+6}]
    set base [expr {2 + $geom(-rectheight)/2}]
    if {!$ll} {
      set v [lindex $legend 0]
      if {$v ne ""} {
        _DrawLegendText $c $textPos $base $geom(-font) $v
      }
    }
    set color [$colorTable GetColor 0]
    set next [expr {$base + $geom(-rectheight)}]
    _DrawLegendRectangleY $c $color $base $next $length
    if {$ll} {
      set v [lindex $opts(-labels) 0]
      set pos [expr {$base + $geom(-rectheight)/2}]
      _DrawLegendText $c $textPos $pos $geom(-font) $v
    }
    set base $next
    set nr [expr {$geom(-size)-2}]
    set n [$colorTable GetSize]
    set q [expr {($n-2) / $nr}]
    # TODO: q == 0?
    for {set i 1} {$i <= $nr} {incr i} {
      set idx [expr {$i*$q}]
      if {$ll} {
        set v [lindex $opts(-labels) $i]
        set pos [expr {$base + $geom(-rectheight)/2}]
        _DrawLegendText $c $textPos $pos $geom(-font) $v
      } else {
        set v [lindex $legend $idx]
        if {$v ne ""} {
          _DrawLegendText $c $textPos $base $geom(-font) $v
        }
      }
      set next [expr {$base + $geom(-rectheight)}]
      set color [$colorTable GetColor $idx]
      _DrawLegendRectangleY $c $color $base $next $length
      set base $next
    }
    # draw last rectangle
    set color [$colorTable GetColor end]
    set next [expr {$base + $geom(-rectheight)}]
    _DrawLegendRectangleY $c $color $base $next $length
    if {$ll} {
      set v [lindex $opts(-labels) end]
      set pos [expr {$base + $geom(-rectheight)/2}]
      _DrawLegendText $c $textPos $pos $geom(-font) $v
    } else {
      set v [lindex $legend end-1]
      if {$v ne ""} {
        _DrawLegendText $c $textPos $base $geom(-font) $v
      }
      set v [lindex $legend end]
      if {$v ne ""} {
        _DrawLegendText $c $textPos $next $geom(-font) $v
      }
    }
  }

  snit::type Table {
    variable colors ""
    
    constructor { args } {
    }
  
    destructor {
    }

    method GetColor { index } {
      return [ lindex $colors $index ]
    }

    method GetSize { } {
      return [ llength $colors ]
    }

    method SetColors { _colors } {
      set colors $_colors
    }
  }

  snit::type Gradient {

    # http://www.personal.psu.edu/cab38/ColorBrewer/ColorBrewer.html
    # http://borel.slu.edu/colors/spanish.html
    typevariable Palettes -array {
      blue1   { \#eff3ff \#08519c }
      green1  { \#ffffcc \#006837 } 
      green2  { \#edf8e9 \#006d2c }
      lemon   { \#edf8e9 \#c0ff00 } 
      gray1   { \#f7f7f7 \#252525 }
      red1    { \#fef0d9 \#b30000 }
      orange1 { \#ffffd4 \#993404 }
      error1  { \#90ff00 \#ff3000 }
      error2  { \#90ff00 \#b30000 }
    }
    
    typemethod GetPalette { name } {
      return $Palettes($name)
    }
    
    variable Table
    
    option -name -default ""
    
    option -from -default "white" -configuremethod _ConfRange
    option -to   -default "black" -configuremethod _ConfRange
    option -size -default 10      -configuremethod _ConfRange
    
    constructor { args } {
      
    $self configurelist $args
    $self _BuildTable
  }
    
    destructor {
    }
    
    method GetSize {} {
      return [llength $Table]
    }

    method GetColor { index } {
      return [ lindex $Table $index ]
    }
    
    method _BuildTable { } {
      set c1 $options(-from)
      set c2 $options(-to)
      set n  $options(-size)
      
      # Color intensities are from 0 to 65535, 2 byte colors.
      if { [llength $c1] == 1 } {
        set c1 [winfo rgb . $c1]
      } 
      if { [llength $c2] == 1 } {
        set c2 [winfo rgb . $c2]
      } 
      foreach {r1 g1 b1} $c1 break
      foreach {r2 g2 b2} $c2 break
      
      #puts "c1: $r1 $g1 $b1"
      #puts "c2: $r2 $g2 $b2"
      
      # Normalize intensities to 0 to 255, 1 byte colors.
      if { $r1 > 255 || $g1 > 255 || $b1 > 255 } {
        foreach el {r1 g1 b1} {
          set $el [expr {[set $el] * 255 / 65535}].0
        }
      }
      if { $r2 > 255 || $g2 > 255 || $b2 > 255 } {
        foreach el {r2 g2 b2} {
          set $el [expr {[set $el] * 255 / 65535}].0
        }
      }
      
      #puts "c1: $r1 $g1 $b1"
      #puts "c2: $r2 $g2 $b2"
      
      if {$n == 1} {
        set r_step 0.0 ; set g_step 0.0 ; set b_step 0.0
      } else {
        set r_step [expr {($r2-$r1) / ($n-1)}]
        set g_step [expr {($g2-$g1) / ($n-1)}]
        set b_step [expr {($b2-$b1) / ($n-1)}]
      }
      
      #puts "$r_step $g_step $b_step"
      
      set Table {}
      for {set i 0} {$i < $n} {incr i} {
        set r [expr {int($r_step * $i + $r1)}]
        set g [expr {int($g_step * $i + $g1)}]
        set b [expr {int($b_step * $i + $b1)}]
        #puts "$r $g $b"
        lappend Table [format "#%.2X%.2X%.2X" $r $g $b]
      }
    }
    
    method _ConfRange { o v } {
      set options($o) $v
      $self _BuildTable
    }
  }
}