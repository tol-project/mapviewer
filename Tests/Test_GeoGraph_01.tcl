source "Test_Conf.tcl"
source "../tcl/RegionShape.tcl"
source "../tcl/GeoGraph.tcl"

set objCOM [ RegionShape %AUTO% \
              -shpfile "$MapPath/spain_regions_ind" \
              -idfield "COM" \
              -namefield "NOMBRE99" \
              -level "Comunidades" ]

set objPROV [ RegionShape %AUTO% \
              -shpfile "$MapPath/spain_provinces_ind_2" \
              -idfield "PROV" \
              -namefield "NOMBRE99" \
              -level "Provincias" ]


set g [ GeoGraph .g ]
pack $g -fill both -expand yes

$g AddRegionShape $objCOM -toplevel 1
$g AddLayer "Comunidades" -label "Comunidades" -color ""

$g AddRegionShape $objPROV

$objPROV DumpHeader
$objPROV DumpEntityInfo 10

for { set i 0 } { $i < [ $objPROV GetNumberOfRecords ] } { incr i } {
  set com [ $objPROV GetFieldValue $i "COM" ]
  if { $com eq "08" } {
    lappend unas_prov [ $objPROV GetFieldValue $i "PROV" ]
  }
}

set unas_prov [ lsort -unique $unas_prov ]

set layer2 [ $g AddLayer "Provincias" \
                 -label "Unas Provincias" \
                 -entities all -color red ]

$g DrawLayer $layer2

$g RaiseLayer $layer2

bind $g <<ActivateEntity>> {
  puts [ $g GetActiveEntity ]
}

$g ZoomAll