package require BWidget

namespace eval MapViewer {
  variable widgets
  variable regionsCache

  array set regionsCache {}
  array set widgets {
    toplevel,counter 0
  }
}

proc MapViewer::GetMapLevels { maps } {
  set script [ string map "%M $maps" {
    NameBlock maps = Eval( "%M" );
    Set maps::GetRegionLevelsID( ? );
  } ]
  return [ lindex [ toltcl::eval $script ] 0 ]
}

proc MapViewer::GetMapInfo { maps level } {
  set script [ string map "%M $maps %L $level" {
    NameBlock maps = Eval( "%M" );
    Set [[
        Text mapsDirectory = maps::GetMapsDirectory(?);
        Set levelInfo = maps::GetRegionLevelInfo( "%L" )
      ]]
  } ]
  return [ lindex [ toltcl::eval $script -named 1 ] 1 ]
}

proc MapViewer::GetRegion { maps level } {
  variable regionsCache
  if { [ info exists regionsCache($maps,$level) ] } {
    return $regionsCache($maps,$level)
  } else {
    return [ CreateRegion $maps $level ]
  }
}

proc MapViewer::CreateRegion { maps level } {
  variable regionsCache

  array set mapsInfo [ GetMapInfo $maps $level ]
  array set levelInfo $mapsInfo(levelInfo)
  set regionsCache($maps,$level) \
      [ RegionShape %AUTO% \
            -shpfile [ file join $mapsInfo(mapsDirectory) $levelInfo(file) ] \
            -idfield $levelInfo(idfield) \
            -namefield $levelInfo(namefield) \
            -level $level ]
}

proc MapViewer::GetInfoFromVariable { ObjAddr } {
  set script [ string map "%A $ObjAddr" {
    NameBlock obj = GetObjectFromAddress( "%A" );
    Set [[
      Text name        = obj::_.name,
      Set regionLevel  = obj::_.regionLevel,
      Set parentLevel  = obj::_.parentLevel,
      Set values       = obj::_.values,
      Set rangeDef     = obj::_.rangeDef,
      Set colorDef     = obj::_.colorDef,
      Set legendDef   = obj::_.legendDef
   ]]
  } ]
  return [ lindex [ toltcl::eval $script -named 1 ] 1 ]
}

proc MapViewer::GetVariableName { ObjAddr } {
  #puts "ObjAddr = $ObjAddr"
  set script [ string map "%A $ObjAddr" {
    NameBlock obj = GetObjectFromAddress( "%A" );
    Text name        = obj::_.name;
  } ]
  return [ toltcl::eval $script ]
}

proc MapViewer::GetVariableMaps { ObjAddr } {
  set script [ string map "%A $ObjAddr" {
    NameBlock obj = GetObjectFromAddress( "%A" );
    Text name        = obj::_.regionLevel["map"];
  } ]
  return [ toltcl::eval $script ]
}

proc MapViewer::GetVariableLevel { ObjAddr } {
  set script [ string map "%A $ObjAddr" {
    NameBlock obj = GetObjectFromAddress( "%A" );
    NameBlock map = Eval( obj::_.regionLevel["map"] );
    Set levelInfo = map::GetRegionLevelInfo( obj::_.regionLevel["level"] );
    Set [[ Text levelClass = levelInfo[ "name" ],
           Text levelSingular = levelInfo[ "singular" ]  ]]
  } ]
  return [ lindex [ toltcl::eval $script ] 0 ]
}

proc MapViewer::__PlotGeoVariable { g ObjAddr } {

  array set objData [GetInfoFromVariable $ObjAddr]
  set ::infoVAR [GetInfoFromVariable $ObjAddr]
  array set varMap $objData(regionLevel)
  set objRegion [ GetRegion $varMap(map) $varMap(level) ]
  if { $objData(parentLevel) eq "" } {
    set topRegion ""
  } else {
    array set parentMap $objData(parentLevel)
    set topRegion [ GetRegion $parentMap(map) $parentMap(level) ]
  }
  set geoVariable [ GeoVariable %AUTO% -name $objData(name) \
                        -regionlevel $varMap(level) ]
  $geoVariable SetValuesDict $objData(values)
  
  $g busy hold
  if { $topRegion ne "" } {
    $g AddRegionShape $topRegion -toplevel 1
  }
  $g AddRegionShape $objRegion
  $g ZoomAll

  array set rangeDef $objData(rangeDef)
  set colorDef(missing) white
  array set colorDef $objData(colorDef)
  set lookupType [ string tolower $colorDef(type) ]

  ### range definition
  set rangeType [ string tolower $rangeDef(type) ]
  if { $rangeType eq "uniform" } { 
    set _rangeDef [ list $rangeType $rangeDef(size) ]
  } elseif { $rangeType eq "quantiles" } {
    set _rangeDef [ list $rangeType $rangeDef(prob) ]
  } elseif { $rangeType eq "nquantiles" } {
    set _rangeDef [ list $rangeType $rangeDef(size) ]
  } elseif { $rangeType eq "absolute" } {
    set points {}
    foreach { {} x } $rangeDef(points) {
      lappend points $x
    }
    set _rangeDef [ list $rangeType $points ]
  } else {
    error "unknown range type '$rangeType', must be: 'uniform', ''quantiles', 'nquantiles' or 'absolute'"
  }
  
  ### color table
  if { $lookupType eq "gradient" } {
    if { [ info exists colorDef(definition) ] } {
      array set palDef $colorDef(definition)
      set palette [ list "gradient" $palDef(from) $palDef(to) ]
    } elseif [ info exists colorDef(palette) ] {
      set palette [ eval list "gradient" \
                        [ RGB::Gradient GetPalette $colorDef(palette) ] ]
    } else {
      error "MapViewer::__PlotGeoVariable unknown gradient specification, must be: 'definition' or 'palette'"
    }
  } elseif { $lookupType eq "table" } {
    set ctable {}
    foreach {{} c} $colorDef(colors) {
      lappend ctable $c
    }
    set palette [ list "table" $ctable ]
  } else {
    error "MapViewer::__PlotGeoVariable unknown type '$lookupType' in _.colorDef, must be 'gradient' or 'table' "
  }

  array set legend {
    fontSize 0
    labels {}
    maxSize 10
  }
  array set legend $objData(legendDef)
  set labels $legend(labels)
  set legend(labels) {}
  foreach {{} l} $labels {
    lappend legend(labels) $l
  }
  $g AddVariable $geoVariable \
      -rangedef $_rangeDef \
      -palette $palette \
      -legendfontsize $legend(fontSize) \
      -legendlabels $legend(labels) \
      -legendsize $legend(maxSize)

  if { $topRegion ne "" } {
    array set parentMapInfo [ GetMapInfo $parentMap(map) $parentMap(level) ]
    array set parentLevelInfo $parentMapInfo(levelInfo)
    set lay1 [ $g AddLayer [ $topRegion cget -level ] \
                   -label $parentLevelInfo(name) -color "" -bordercolor red ]
    $g DrawLayer $lay1
    update
  }

  array set varMapInfo [ GetMapInfo $varMap(map) $varMap(level) ]
  array set varLevelInfo $varMapInfo(levelInfo)

  set lay2 [$g AddLayer [$objRegion cget -level] \
                -label "${objData(name)}/$varLevelInfo(name)" \
                -color [list "variable" [$geoVariable cget -name]] \
                -missingcolor $colorDef(missing)]

  $g DrawLayer $lay2
  
  if { $topRegion ne "" } {
    $g RaiseLayer $lay1
  }
  $g busy release
}

proc MapViewer::OnDestroyGraph { g } {
  variable widgets
  
  if {[info exists widgets(frame,$g)]} {
    array unset widgets frame,$g
  }
}

proc MapViewer::OnCursorMove { g } {
  variable widgets
  
  set f $widgets(frame,$g)
  set xy [$g GetLastPosition]
  $f.labPosValue configure -text [join $xy ","]
}

proc MapViewer::OnActivateEntity { g } {
  variable widgets

  set id [ $g GetActiveEntity ]
  if { $id ne "" } {
    set f $widgets(frame,$g)
    set region [ $g GetActiveRegionShape ]
    if { $region ne "" } {
      set entityName [ $region GetEntityName $id ]
      if { $entityName eq "UNNAMED" } {
        set entityName $id
      }
      $f.labEntity configure -text $entityName
      set value [ $g GetActiveVariableValue ]
      if {$value ne "" && [string is double $value]} {
        set value [format %g $value]
      } 
      $f.labVarValue configure -text $value
    }
  }
}

proc MapViewer::PlotGeoVariable { ObjAddr } {
  variable widgets

  set varName [ GetVariableName $ObjAddr ]
  foreach { varLevel levelSingular } [ GetVariableLevel $ObjAddr ] break
  #set useMaps [ GetVariableMaps $ObjAddr ]
  if { [ llength [ info command ::project::CreateForm ] ] } {
    set mdi [ ::project::CreateForm \
                  -title [ mc "%1s on %2s" $varName $varLevel ] \
                  -type MapViewer -iniconfig MapViewer ]
    set w [ $mdi getframe ]
  } else {
    set id [ incr widgets(toplevel,counter) ]
    set w .t$id
    destroy $w
    toplevel $w
    wm geometry $w 700x600
  }
  PanedWindow $w.paned -side top 
  # -weights available -activator line
  set fg [ $w.paned add -weight 3 ]
  set fi [ $w.paned add -weight 1 ]
  label $fi.labLevel -text "$levelSingular:"
  label $fi.labEntity  -bg gray90 -relief solid -borderwidth 1 \
      -padx 2 -pady 2 -anchor w
  label $fi.labVar -text "$varName:"
  label $fi.labVarValue -bg gray90 -relief solid -borderwidth 1 \
      -padx 2 -pady 2 -anchor w
  label $fi.labPosValue -anchor w
  grid $fi.labLevel    -row 0 -column 0 -sticky "ws"
  grid $fi.labEntity   -row 1 -column 0 -sticky "wn"
  grid $fi.labVar      -row 2 -column 0 -sticky "ws"
  grid $fi.labVarValue -row 3 -column 0 -sticky "wn"
  grid $fi.labPosValue -row 10 -column 0 -sticky "wn"
  grid rowconfigure $fi 0 -pad 5
  grid rowconfigure $fi 1 -pad 5
  grid rowconfigure $fi 4 -weight 1
  grid columnconfigure $fi 1 -weight 1
  set g [GeoGraph $fg.g]
  set widgets(frame,$g) $fi
  grid $g -row 0 -column 0 -sticky "snew"
  grid rowconfigure $fg 0 -weight 1
  grid columnconfigure $fg 0 -weight 1
  grid $w.paned -row 0 -column 0 -sticky "snew"
  grid rowconfigure $w 0 -weight 1
  grid columnconfigure $w 0 -weight 1
  
  bind $g <Destroy> [list MapViewer::OnDestroyGraph %W]
  bind $g <<ActivateEntity>> [list MapViewer::OnActivateEntity %W]
  bind $g <<CursorMove>> [list MapViewer::OnCursorMove %W]
  update 

  __PlotGeoVariable $g $ObjAddr
  $g UpdateLegend
}
