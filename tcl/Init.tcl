#puts "auto_path = $::auto_path";

puts "inicializando ..."

namespace eval :: { 
  set cwd [ file normal [ file dir [ info script ] ] ]
  puts "$cwd"
  lappend ::auto_path [ file normal [ file join $cwd .. lib ] ]
  if {![namespace exists toltcl]} {
    source [ file join $cwd toltcl.tcl ]
  }
  source [ file join $cwd MapViewer.tcl ]
  source [ file join $cwd GeoGraph.tcl ]
  source [ file join $cwd GeoVariable.tcl ]
  source [ file join $cwd RegionShape.tcl ]
}
