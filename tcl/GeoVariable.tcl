package require snit

snit::type GeoVariable {
  option -regionlevel -default "" -readonly 1
  option -name        -default "" -readonly 1

  variable Values -array ""
  
  constructor { args } {
    $self configurelist $args
  }

  destructor {
  }

  method GetValue { id } {
    return $Values($id)
  }

  method GetRawValues { } {
    foreach idx [ array names Values ] {
      set v $Values($idx)
      if {$v ne "?"} {
        lappend values $Values($idx)
      }
    }
    return $values
  }
  
  method SetValue  { id value } {
    set Values($id) $value
  }
  
  method SetValuesDict { _values } {
    array set Values $_values
  }

  method SetValuesRec { _values } {
    foreach iv $_values {
      $self SetValue [ lindex $iv 0 ] [ lindex $iv 1 ]
    }
  }

  method GetEntityNames { } {
    return [ array names Values ]
  }
  
  method GetRegionLevel { } {
    return [ $self cget -regionlevel ]
  }
}