set _cwd [ file normal [ file dir [ info script ] ] ]
source [ file join $_cwd "Test_Conf.tcl" ]
source [ file join $_cwd  ".." "tcl" "RegionShape.tcl" ]
source [ file join $_cwd  ".." "tcl" "GeoGraph.tcl" ]

set objMUN [ RegionShape %AUTO% \
              -shpfile "$MapPath/esp_muni_00" \
              -idfield "PROVMUN" \
              -namefield "MUNICIPIO0" \
              -level "Municipios" ]

set g [ GeoGraph .g ]
pack $g -fill both -expand yes

update 

$g busy hold

$g AddRegionShape $objMUN

set lay1 [ $g AddLayer "Municipios" \
               -label "Municipios" \
               -entities all -color \#c0ff00 -outline gray50 ]
$g DrawLayer $lay1
update
$g busy release
$g ZoomAll
