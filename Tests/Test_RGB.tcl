set _cwd [ file normal [ file dir [ info script ] ] ]
source [ file join $_cwd .. "RGB.tcl" ]

RGB::Gradient GetPalette "green1"

set palette1 [ RGB::Gradient GetPalette error1 ]
set palette2 [ RGB::Gradient GetPalette error2 ]

#RGB::Gradient grad1 -from {65535 65535 45568} -to {48384 0 9728}
#RGB::Gradient grad1 -from \#edf8e9 -to \#90ff00
RGB::Gradient grad1 -from [ lindex $palette1 0 ] -to [ lindex $palette1 1 ]
#RGB::Gradient grad2 -from "yellow" -to "red"
RGB::Gradient grad2 -from [ lindex $palette2 0 ] -to [ lindex $palette2 1 ]

set palette { \#eff3ff \#08519c }
RGB::Gradient grad3 \
    -from [ lindex $palette 0 ] -to [ lindex $palette 1 ]

proc paint { c type grad } {
  $c delete gradient
  set w [ winfo width $c ]
  set h [ winfo height $c ]
  if { $type eq "x" } {
    $grad configure -size $w
    for { set i 0 } { $i < $w } { incr i } {
      $c create line $i 0 $i $h -tags gradient -fill [ $grad GetColor $i ]
    }
  } else {
    $grad configure -size $h
    for { set i 0 } { $i < $h } { incr i } {
      $c create line 0 $i $w $i -tags gradient -fill [ $grad GetColor $i ]
    }
  }
}

destroy .c1
destroy .c2
canvas .c1
canvas .c2
bind .c1 <Configure> [ list paint %W x grad1 ]
bind .c2 <Configure> [ list paint %W y grad2 ]
pack .c1 .c2 -fill both -expand 1
